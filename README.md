# MeituAI Demo

## How to Install
You need Python 3
```
pip install requests

python face.py

python hair.py
```

## Documents

This service is provided by a Chinese company called Meitu, They released some very famous photo and beauty apps in China. So you can use Google translation during visiting.

### Face analysis
https://ai.meitu.com/doc?id=49&type=api&lang=zh

### Hair analysis
https://ai.meitu.com/doc?id=79&type=api&lang=zh

## Limitation
Feel free to request these APIs but their free times are only 1000 times and will be expired after 2020-06-11. So try to minimize testing. If we need more, I can apply for payment, their price about 0.0056 pounds once.